local opt = {ip = 'public'}
local delimiter = ' | '
local colors = {black = '#1f222d', dimmed = '#6b6b6b', orange = '#FF6200', white = '#ebdbb2'}

local utils = require('tms.barutils')
local monitors = utils.monitors()

-- components
-- BSPWM
local function bspwm_st()
  local report = io.popen('bspc subscribe report')
  local raw = report:read('l')
  report:close()
  utils.D('%s', raw)

  -- parse
  local raw_monitors = string.match(raw, '^W(.*)')
  local bspwm_structure = {}
  local monitor
  local function add_monitor(monitor) if monitor then bspwm_structure[monitor.name] = monitor end end
  for i in string.gmatch(raw_monitors, '[^:]+') do
    local char = string.sub(i, 1, 1)
    local item = string.sub(i, 2)

    if char == 'm' then -- monitors
      add_monitor(monitor)
      monitor = {name = item, focus = false, desktops = {}}
    elseif char == 'M' then
      add_monitor(monitor)
      monitor = {name = item, focus = true, desktops = {}}
    elseif not monitor then
      -- do not continue
    elseif char == 'o' then -- Ocupied
      table.insert(monitor.desktops, {name = item, focus = false, free = false})
    elseif char == 'O' then
      table.insert(monitor.desktops, {name = item, focus = true, free = false})
    elseif char == 'f' then -- Free
      table.insert(monitor.desktops, {name = item, focus = false, free = true})
    elseif char == 'F' then
      table.insert(monitor.desktops, {name = item, focus = true, free = true})
    end
  end
  -- add last monitor
  add_monitor(monitor)

  return bspwm_structure
end

local function bspwm_build(structure, monitor_name)
  local monitor = structure[monitor_name]
  local mfocus = monitor.focus
  local build = {}
  for _, desktop in ipairs(monitor.desktops) do
    local dformat = string.format('  %s  ', desktop.name)
    local dfree = desktop.free
    local dfocus = desktop.focus

    if mfocus and dfocus then
      dformat = utils.w.col(dformat, colors.black, colors.orange)
    elseif not mfocus and dfocus then
      dformat = utils.w.col(dformat, colors.black, colors.white)
    elseif dfree then
      dformat = utils.w.col(dformat, colors.dimmed)
    elseif not dfree then
      dformat = utils.w.u(dformat)
    end

    table.insert(build, dformat)
  end
  return table.concat(build)
end

local bspwm = {}
for _, monitor in ipairs(monitors) do
  bspwm[monitor.name] = {
    name = 'bspwm' .. monitor.name,
    func = function() return bspwm_build(bspwm_st(), monitor.name) end,
  }
end

-- Components
local c = {}
c.bspwm = {
  actions = {
    bspwm = function(u)
      utils.D('bspwm')
      for _, monitor in ipairs(monitors) do u.updateComponent('bspwm' .. monitor.name) end
    end,
  },
}
c.volume = {
  name = 'volume',
  func = function()
    local text = utils.cmd_line('audio output | sed \'/^$/d\' ')
    return utils.w.btns(text, {sink = 1, mute = 2, volup = 4, voldown = 5})
  end,
  suffix = delimiter,
  actions = {
    volume = function(u) u.updateComponent('volume') end,
    sink = function(_) os.execute('audio next-sink &') end,
    volup = function(_) os.execute('audio up &') end,
    voldown = function(_) os.execute('audio down &') end,
    mute = function(_) os.execute('audio togmute &') end,
  },
}
c.mail = {
  name = 'mail',
  func = function()
    local text = utils.cmd_line('mail_count.lua')
    if text and text ~= '' then return string.format('%%{A:neomutt:}%s%%{A}', text) end
    return ''
  end,
  suffix = delimiter,
  actions = {
    mail = function(u) u.updateComponent('mail') end,
    neomutt = function(_) os.execute('alacritty -e neomutt') end,
  },
}
c.layout = {
  name = 'layout',
  func = function()
    local text = utils.cmd_line([[getxkblayout | sed -n '1p' | sed -n 's/.*:\(.*\)/\1/p']])
    return utils.w.btn(text, 'layoutswitch')
  end,
  suffix = delimiter,
  actions = {
    layout = function(u) u.updateComponent('layout') end,
    layoutswitch = function(_) os.execute('xkb-switch -n') end,
  },
}
c.ip = {
  name = 'ip',
  func = function()
    if opt.ip == 'public' then
      return utils.w.btns(utils.cmd_line('publicip'), {ip = 1, ipcp = 3})
    elseif opt.ip == 'local' then
      return utils.w.btns(utils.cmd_line('publicip --local ' .. os.getenv('ETH')), {ip = 1, ipcp = 3})
    end
  end,
  suffix = delimiter,
  actions = {
    ipcp = function(_)
      local ip
      if opt.ip == 'public' then
        ip = utils.cmd_line('publicip')
      elseif opt.ip == 'local' then
        ip = utils.cmd_line('publicip --local ' .. os.getenv('ETH'))
      end
      if ip then os.execute(string.format('echo \'%s\' | xclip -sel clip', ip)) end
    end,
    ip = function(u)
      if opt.ip == 'public' then
        opt.ip = 'local'
      else
        opt.ip = 'public'
      end
      u.updateComponent('ip')
    end,
  },
}
c.date = {
  name = 'date',
  interval = 1,
  func = function() return utils.w.btn(utils.cmd_line('date +%d.%m.%Y\\ %H:%M:%S'), 'calendar'), colors.white end,
  suffix = '  ',
  actions = {calendar = function(_) os.execute('$BROWSER --new-window https://calendar.google.com') end},
}
c.toggl = {
  name = 'toggl',
  interval = 1,
  func = function() return utils.cmd_line('toggl_status -f ~/.cache/toggl_track') end,
}
c.timer = {
  name = 'timer',
  func = function()
    local text = utils.cmd_line('timer_bar')
    return utils.w.btn(text, 'timerrm')
  end,
  interval = 1,
  actions = {
    timer = function(u) u.updateComponent('timer') end,
    timerrm = function(u)
      os.execute('timer rm')
      u.updateComponent('timer')
    end,
  },
}
c.trun = {
  name = 'trun',
  prefix = ' ',
  func = function() return utils.cmd_line('trun-fmt') end,
  actions = {trun = function(u) u.updateComponent('trun') end},
}

-- Structure
local format = {
  -- primary monitor
  primary = {c.bspwm, '%{c}', c.timer, c.toggl, c.trun, '%{r}', c.mail, c.volume, c.layout, c.ip, c.date},
  -- secondary monitor
  secondary = {'%{r}', c.volume, c.ip, c.date},
}

local M = {}
for _, monitor in pairs(monitors) do
  M[monitor.name] = {}
  table.insert(M[monitor.name], bspwm[monitor.name])
  if monitor.primary then
    for _, component in ipairs(format.primary) do table.insert(M[monitor.name], component) end
  else
    for _, component in ipairs(format.secondary) do table.insert(M[monitor.name], component) end
  end
end
return M
