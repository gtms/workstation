local profile = require('tms.neomutt.profile')
local utils = require('tms.neomutt.utils')
local set = utils.set
local unset = utils.unset
local bind = utils.bind
local unbind = utils.unbind
local macro = utils.macro
local cmd = mutt.command

-- if os.getenv('SCHEME') then
--   local theme =
--     io.open(os.getenv('THEME') or os.getenv('CONFIG') .. '/theme'):read();
-- mutt.command.source(string.format('~/.config/neomutt/colors/%s-%s',
--                                   os.getenv('SCHEME'), theme))
-- end

-- quad
local q = {}
q.yes = mutt.QUAD_YES
q.askyes = mutt.QUAD_ASKYES
q.askno = mutt.QUAD_ASKNO
q.no = mutt.QUAD_NO

-- paths
local path = {}
path.mailcap = '~/.config/neomutt/mailcap'
path.temp = '~/.cache/neomutt'
path.header_cache = '~/.cache/mutt'

local query_cmd = 'goobook query %s'

-- Settings {{{
-- commands
cmd.auto_view('text/html', 'text/calendar', 'application/ics')
cmd.alternative_order('text/plain', 'text/html', 'text/enriched', 'text/*')

-- MAIL
set 'edit_headers' -- edit header of outgoing msg along with body
set {'forward_format', 'Fwd: %s'} -- format of subject when forwarding
set 'forward_decode' -- decode when forwarding
set 'forward_quote' -- indent forwarded message
-- o.text_flowed = true -- wtf
unset 'mime_forward' -- forward attachments as part of body
set {'include', q.yes} -- include message in replies
set {'reply_to', q.yes} -- reply to Reply to: field
set {'fcc_attach', q.yes} -- save attachments with the body
set 'reverse_name' -- reply as whomever it was to
set {'send_charset', 'utf-8:iso-8859-1:us-ascii'}
set {'charset', 'utf-8'}
unset 'record'

-- EDITOR
set {'editor', 'nvim -c"set textwidth=0" -c"set ft=mail"'}
unset 'help' -- No help bar at the top of index
set {'tmpdir', path.temp} -- where to keep temp files
unset 'resolve' -- do not advance after command execution to next message
unset 'mark_old' -- disable old messages
set 'pipe_decode' -- strip headers and eval mimes when piping
set 'thorough_search' -- strip headers and eval mimes before searching
unset 'arrow_cursor' -- Change `color indicator` depending
set {'sort', 'threads'} -- sort index
set {'sort_aux', 'reverse-last-date-received'} -- sort replies
set 'sort_re' -- add reply messages to try by subject regexp
-- set "uncollapse_jump"
set {'pager_index_lines', 10} -- show X lines of index while pager open
set 'menu_scroll' -- scrol by line rather than by page
set 'tilde' -- show tildes after message (like in vim)
set {'mailcap_path', path.mailcap}
set {'header_cache', path.header_cache}
set {'header_cache_backend', 'bdb'}
set {'query_command', query_cmd}
-- formats
set {'status_chars', '-*%A'} -- for status foramt '%r' (unchanged,modified,readonly,attachedmode)
--[[ set {
  "status_format",
  "[ Folder: %f ] [%r%m messages%?n? (%n new)?%?d? (%d to delete)?%?t? (%t tagged)? ]%>─%?p?( %p postponed )?",
} ]]
set {'date_format', '%d.%m.%y %H:%M'}
set {'index_format', '%5C [%Z] %D %-20L (%?l?%4l&%4c?) %s'}
-- speed
set {'abort_key', '<esc>'} -- abort prompt
set {'fast_reply', q.yes} -- skip to compose when replying
set {'autoedit', q.yes} -- skip to compose
unset 'wait_key' -- on external command do not wait
set {'timeout', 0} -- do not wait for input
set {'sleep_time', 0} --  do not wait
unset 'confirmappend' --  do not ask
set 'auto_subscribe' -- mailing lists
set {'delete', q.yes} -- do not ask
-- }}}

-- SIDEBAR {{{
-- set {"sidebar_visible", "no"} -- Should the Sidebar be shown?
set {'sidebar_width', 20} -- How wide should the Sidebar be in screen columns?
-- set {"sidebar_new_mail_only", "no"} -- Make the Sidebar only display mailboxes that contain new, or flagged, mail.
-- set {"sidebar_non_empty_mailbox_only", "no"} -- Any mailboxes that are whitelisted will always be visible, even if the sidebar_new_mail_only option is enabled.
-- set {"sidebar_next_new_wrap", "no"} -- When searching for mailboxes containing new mail, should the search wrap around when it reaches the end of the list?
set {'sidebar_format', '%B%?F? [%F]?%* %?N?%N/?%S'} -- Display the Sidebar mailboxes using this format string.
-- }}}

-- KEYBIND  {{{
-- reset
unbind('*')
bind('ibpa', 'g', 'noop')
bind('ibpac', '!', 'shell-escape')
bind('ibpac', '\\;', 'tag-prefix')
bind('ibpac', 'g?', 'help')

-- movement {{{
bind('ibpac', 'j', ' next-entry')
bind('ibpac', 'k', 'previous-entry')
bind('ibpac', 'l', 'select-entry')
bind('ibpac', 'gg', 'first-entry')
bind('ibpac', 'G', 'last-entry')
bind('ibpac', 'zt', 'current-top')
bind('ibpac', 'zz', 'current-middle')
bind('ibpac', 'zb', 'current-bottom')
bind('ibpac', '\\cy', 'previous-line')
bind('ibpac', '\\ce', 'next-line')
bind('ibpac', '\\cd', 'half-down')
bind('ibpac', '\\cu', 'half-up')
bind('ibpac', '\\cb', 'previous-page')
bind('ibpac', '\\cf', 'next-page')
bind('ibpac', '/', 'search')
bind('ibpac', '?', 'search-reverse')
bind('ibpac', 'n', 'search-next')
bind('ibpac', 'N', 'search-opposite')
bind('ibpac', 'H', 'top-page')
bind('ibpac', 'M', 'middle-page')
bind('ibpac', 'L', 'bottom-page')
bind('ibpac', 'h', 'exit')
-- marks
for _, l in ipairs({
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
}) do macro('ibac', 'm' .. l, '<mark-message>' .. l .. '<enter>') end
-- -- index
macro('i', 'h', '<change-folder>?')
-- -- browser
bind('b', 'L', 'descend-directory')
bind('b', 'H', 'goto-parent')
-- -- pager
bind('p', 'k', 'previous-line')
bind('p', 'j', 'next-line')
bind('p', 'J', 'next-entry')
bind('p', 'K', 'previous-entry')
bind('p', 'gg', 'top')
bind('p', 'G', 'bottom')
-- }}}

-- multi

-- specific {{{
bind('ica', 't', 'tag-entry')
macro('i', 'A', '<tag-pattern>~N<enter><tag-prefix><clear-flag>N<untag-pattern>.<enter>', 'read all')
bind('i', 'l', 'display-message')
bind('i', '@', 'display-address')
bind('i', 'd', 'delete-message')
bind('i', 'x', 'purge-message')
macro('i', 's', '<save-message>?<toggle-mailboxes>') -- Move message to ?
bind('i', 'D', 'delete-pattern')
bind('i', 'gd', 'undelete-pattern')
bind('i', 'u', 'undelete-message')
bind('i', 'D', 'delete-thread')
bind('i', 'U', 'undelete-thread')
bind('i', '<space>', 'collapse-thread')
bind('i', 'T', 'tag-pattern')
bind('i', '\\ct', 'untag-pattern')
bind('i', 'w', 'set-flag')
bind('i', 'W', 'clear-flag')
bind('ip', '\\cr', 'sync-mailbox')
bind('p', 'l', 'view-attachments')
bind('b', 'R', 'catchup')
bind('p', 'H', 'display-toggle-weed')
bind('ca', 'v', 'view-attach')
bind('ac', '\\ct', 'edit-type')
bind('a', 'o', 'view-mailcap')
bind('a', 's', 'save-entry')
bind('a', 'p', 'pipe-entry')
bind('a', 'd', 'delete-entry')
bind('a', 'u', 'undelete-entry')
bind('a', '<space>', 'collapse-parts')
macro('c', 'a',
      '<shell-escape>bash $HOME/.config/neomutt/muttvifm.sh<enter><enter-command>source /tmp/muttattach<enter><shell-escape>bash $HOME/.config/neomutt/muttvifm.sh clean<enter>') -- attachments
bind('c', 'A', 'attach-message')
bind('c', 's', 'copy-file') -- save
bind('c', 'd', 'detach-file')
bind('c', '\\ck', 'move-up')
bind('c', '\\cj', 'move-down')
bind('c', 'p', 'pipe-entry')
bind('c', 'e', 'edit-message')
bind('c', '<return>', 'edit-message')
bind('c', 'r', 'rename-attachment')
bind('c', 'f', 'rename-file')
bind('c', 'y', 'send-message')

-- mailing
bind('ip', 'r', 'reply')
bind('ip', 'f', 'bounce-message')
bind('ip', 'R', 'resend-message')
bind('ip', '\\#', 'delete-thread')
bind('ip', 'c', 'mail')

-- editor
bind('e', '<Tab>', 'complete-query')

-- sidebar
bind('ip', '\\Cj', 'sidebar-next')
bind('ip', '\\Ck', 'sidebar-prev')
macro('ip', 'S', '<sidebar-open><sidebar-toggle-visible>')

macro('ip', 'o', '<view-attachments><search>html<enter><view-mailcap><exit>') -- Open  html
macro('ip', 'e', '<view-attachments><search>plain<enter><view-mailcap><exit>') -- Open  plain
macro('ipa', ',s', '<pipe-message>urlscan -r "setsid -f brave --new-window {}"<enter>') -- Search urls
-- }}}
-- }}}

-- PROFILES {{{

local gpg_opts = {
  pgp_default_key = '2C22CDB78898C83080A562F4F5BE63822460230F',
  crypt_use_gpgme = q.yes,
  postpone_encrypt = q.yes,
  pgp_self_encrypt = q.yes,
  crypt_autosign = q.yes,
  pgp_sign_as = '0xF5BE63822460230F',
}

-- helpers
local active_path = ''
local function expand_path(folder) return active_path .. '/' .. folder end

-- templates
local gmail_template = {
  options = {
    mbox_type = 'Maildir',
    use_from = true,
    use_envelope_from = true,
    spoolfile = expand_path('INBOX'),
    postponed = expand_path('drafts'),
    trash = expand_path('trash'),
  },
  mailboxes = {'=INBOX', '=archive', '=drafts', '=junk', '=sent', '=starred', '=trash'},
}

-- profiles
-- MEDORO
active_path = '/home/tms/.mail/medoro'
ProfileMedoro = profile:new('medoro', {
  options = {
    realname = 'Tomáš Němec',
    from = 'nemec@medoro.org',
    sendmail = '/usr/bin/msmtp -a medoro',
    folder = '~/.mail/medoro',
  },
  -- binds = {{"ip", "x", "purge-message"}},
  macros = {{'ip', '$', '<shell-escape>mbsync medoro<enter>'}},
}, gmail_template)

-- NEMI
active_path = '/home/tms/.mail/nemi'
local nemi_opts = {
  realname = 'Tomáš Němec',
  from = 'nemi@skaut.cz',
  sendmail = '/usr/bin/msmtp -a nemi',
  folder = '~/.mail/nemi',
}

-- for k, v in pairs(gpg_opts) do
--   nemi_opts[k] = v
-- end
ProfileNemi = profile:new('nemi', {
  options = nemi_opts,
  -- binds = {{"ip", "x", "purge-message"}},
  macros = {{'ip', '$', '<shell-escape>mbsync nemi<enter>'}},
}, gmail_template)

-- GTMS
active_path = 'home/tms/.mail/gtms'
ProfileGtms = profile:new('gtms', {
  options = {
    realname = 'Tomáš Němec',
    from = 'tms@gtms.dev',
    sendmail = '/usr/bin/msmtp -a gtms',
    folder = '~/.mail/gtms',
  },
  macros = {{'ip', '$', '<shell-escape>mbsync gtms<enter>'}},
}, {
  options = {
    mbox_type = 'Maildir',
    use_from = true,
    use_envelope_from = true,
    spoolfile = expand_path('INBOX'),
    postponed = expand_path('Drafts'),
    trash = expand_path('Trash'),
  },
  mailboxes = {'=INBOX', '=Archive', '=Drafts', '=Spam', '=Sent', '=Trash'},
})

macro('ip', 'gn', '<enter-command>lua ChangeProfile(ProfileNemi)<enter>')
macro('ip', 'gm', '<enter-command>lua ChangeProfile(ProfileMedoro)<enter>')
macro('ip', 'gt', '<enter-command>lua ChangeProfile(ProfileGtms)<enter>')

local active_profile
function ChangeProfile(p)
  if active_profile then active_profile:unload() end
  p:load()
  cmd.push('<change-folder>=INBOX<enter>')
  set {
    'status_format',
    string.format(
      '[%s][ Folder: %%f ] [%%r%%m messages%%?n? (%%n new)?%%?d? (%%d to delete)?%%?t? (%%t tagged)? ]%%>─%%?p?( %%p postponed )?',
      p.name),
  }
  active_profile = p
end

-- first load
ChangeProfile(ProfileNemi)
-- }}}
