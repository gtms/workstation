
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
;orange = #FF6200
;orange = #d65d0e
darkgray = ${xrdb:color8}
orange = ${xrdb:color9}
white = #ebdbb2
gray = #585858
black = #090909
red = #c795ae
blue = #95aec7
yellow = #c7ae95
green = #aec795
#background = #1f222d
background = #262626
background-alt = #4e4e4e
#foreground = #dfdfdf
foreground = ${xrdb:foreground}
foreground-alt = #4e4e4e
primary = #1f222d
secondary = #FF6200
alert = #fb4934

[bar/base]
width = 100%
height = 24
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
font-0 = JetBrainsMono Nerd Font:pixelsize=11;1
dpi=72
enable-ipc = true
wm-restack = bspwm
cursor-click = pointer

[bar/secondary]
monitor = ${env:MONITOR:}
inherit = bar/base
modules-left = bspwm
modules-right = xkeyboard publicip date

[bar/primary]
monitor = ${env:MONITOR:}
inherit = bar/base
modules-left = bspwm
modules-center = timer trun toggl
modules-right = neomutt xkeyboard publicip date
tray-position = right
tray-padding = 2

[module/pulseaudio-control]
type = custom/script
tail = true
label = %output%
format-underline = ${colors.white}

exec = pulseaudio-control listen
click-right = exec pavucontrol &
click-left = pulseaudio-control togmute
click-middle = pulseaudio-control next-sink
scroll-up = pulseaudio-control up
scroll-down = pulseaudio-control down
label-padding = 2
label-foreground = ${colors.foreground}

[module/bspwm]
type = internal/bspwm
pin-workspaces = true
enable-click = true
enable-scroll = false
fuzzy-match = true

label-monitor = %name%

label-dimmed-focused-underline = ${colors.white}
label-dimmed-focused-background = ${colors.white}
label-dimmed-focused-foreground = ${colors.black}

label-focused = %name%
label-focused-background = ${colors.secondary}
label-focused-foreground = ${colors.black}
label-focused-underline = ${colors.secondary}
label-focused-padding = 2

label-occupied = %name%
label-occupied-underline = ${colors.white}
label-occupied-padding = 2

label-urgent = %name%
label-urgent-background =${colors.alert}
label-urgent-padding = 2

label-empty = %name%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; label-separator = |
; label-separator-padding = 0
; label-separator-foreground = ${xrdb:polybar.bspwm.separator:#ffb52a}

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

; format-prefix-underline = ${xrdb:polybar.secondary}

label-layout = %name%
; label-layout-underline = ${xrdb:polybar.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
; label-indicator-background = ${xrdb:polybar.secondary}
; label-indicator-underline = ${xrdb:polybar.secondary}
;
[module/rss]
type = custom/script
exec = newrss update polybar
interval = 5

[module/monitor]
type = custom/script
exec = echo $MONITOR

[module/neomutt]
type = custom/ipc
hook-0 = mail_count.lua
initial = 1
click-left = alacritty -e neomutt
click-middle = polybar-msg -p %pid% hook neomutt 1
format-underline = ${colors.blue}

[module/toggl]
type = custom/script
exec = toggl_polybar -f ~/.cache/toggl_track
interval = 1
click-middle = toggl www
format-underline = ${colors.white}

[module/cpu]
type = internal/cpu
interval = 2
format-underline = ${colors.white}
label = CPU %percentage%%

[module/memory]
type = internal/memory
interval = 2
format-underline = ${colors.white}
label = MEM %gb_free%

[module/publicip]
type = custom/ipc
hook-0 = publicip --local $ETH
hook-1 = publicip --public
initial = 2
click-left = polybar-msg -p %pid% hook publicip 1
click-right = polybar-msg -p %pid% hook publicip 2
format-underline = ${colors.white}

[module/timer]
type = custom/script
exec = timer_bar
interval = 1
format-underline = ${colors.white}
click-left = timer rm -

[module/trun]
type = custom/ipc
hook-0 = track_run_status
initial = 1
format-underline = ${colors.white}
click-left = polybar-msg -p %pid% hook trun 1

[module/date]
type = internal/date
interval = 5

date = "%d.%m.%Y"

time = %H:%M
time-alt = %H:%M:%S

format-underline = ${colors.white}

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio
use-ui-max = false

format-volume = <label-volume>
format-volume-underline = ${colors.white}

format-muted-underline = ${colors.white}

label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = VOL muted
; label-muted-foreground = ${xrdb:polybar.volume.muted:#666}

[module/temperature]
type = internal/temperature
thermal-zone = 0
base-temperature = 20
warn-temperature = 60

hwmon-path = /sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon3/temp1_input

format = <label>
format-underline = ${colors.white}
format-warn = <label-warn>
format-warn-underline = ${self.format-underline}

label = TEMP %temperature-c%
label-warn = %temperature-c%
; label-warn-foreground = ${xrdb:polybar.secondary}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
