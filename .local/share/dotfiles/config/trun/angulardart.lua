--- Trun module handler for `pub run build_runner serve`
-- NOTE: This does not work for `webdev server`
--
-- Status number i use for semaphor in WM status bar.
-- viz->tools/print_status.lua
--
-- Last output is inside tmpfile so i can load errors to quickfix list
-- inside vim.
-- viz->examples/handler_temp_output_file.lua -> for caching last output
-- viz->tools/trun_to_neovim_quickfix.lua -> for load qf-list
--
--
local s_map = {['running'] = 0, ['success'] = 1, ['severe'] = -1}

local nvim_cmd = function(nvim_cmd)
  local servers = io.popen('nvr --serverlist')
  for socket in servers:lines() do
    local cmd = string.format('nvr --servername "%s" -cc "%s" --nostart -s &',
                              socket, nvim_cmd)
    os.execute(cmd)
  end
  servers:close()
end

local nvim_notify = function(msg, title, level)
  local cmd = string.format(
                'lua require(\'notify\')(\'%s\', \'%s\', {title=\'%s\'})', msg,
                level, title)
  nvim_cmd(cmd)
end

local last_line
local ls
local notify = function(name, status)
  os.execute('bar-update trun')
  if not status or ls == status then
    return
  end
  if status == 1 or status == -1 then
    local level_map = {[1] = 'info', [-1] = 'error'}
    if not last_line then
      local level_msg = {[1] = 'Succeeded', [-1] = 'Failed'}
      last_line = level_msg[status]
    end
    nvim_notify(last_line, name, level_map[status])
  end
  ls = status
end

-- Output types:
-- [SEVERE] Failed after 11.4s
-- [INFO] Succeeded after 9.8s with 304 outputs (8 actions)
--
-- Sometimes build is not ending with these lines. There may be couple of empty
-- lines after.

local succeeded = function(line)
  if line:match('%[INFO%] Succeeded') then
    return true
  elseif line:match('Serving') then
    return true
  end
end

local last_status
local get_status = function(line)
  if last_status and #line == 0 then
    return last_status
  end
  if (succeeded(line)) then
    last_line = line
    return s_map.success
  end

  local line_type = line:match('^%[(.*)%] .*');
  if not line_type then
    return s_map.running
  end

  if (line_type == 'SEVERE') then
    last_line = line
    return s_map.severe
  end

  return s_map.running
end

return {

  -- on_start(userdata): before start reading
  on_start = function(data)
    data.tmpfile = '/tmp/' .. data.name .. '.trun'
    os.execute('rm -f ' .. data.tmpfile .. ' 2>/dev/null')
  end,

  -- handle(userdata, line): return status
  handle = function(data, line)
    local tmpfile = io.open(data.tmpfile, 'a')
    tmpfile:write(line, '\n')
    tmpfile:close()
    local status = get_status(line)
    last_status = status
    return {status, data.tmpfile}
  end,

  -- on_update(userdata, line, status): when status changed
  on_update = function(data, _, status)
    if status[1] == s_map.success then
      io.open(data.tmpfile, 'w'):close()
    end
    notify(data.name, status[1])
  end,

  -- on_end(userdata): after reading end
  on_end = function(data)
    notify()
    os.execute('rm -f ' .. data.tmpfile)
  end,
}
