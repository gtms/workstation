--- Trun module handler for `pub run`
--
-- interface
-- handle: return status that is written to a file
-- (optional) onStart: call once before start reading
-- (optional) onUpdate: calls every time when stdin reads a line
-- (optional) onEnd: calls once after stream end
local M = {}

-- handler
local ds = {['running'] = 0, ['success'] = 1, ['severe'] = -1}

-- update
local function notifybar() os.execute('bar-update trun') end

-- [INFO] Succeeded after 29.8s with 304 outputs (8 actions)
local function isSucceed(line)
  if line:match('%[INFO%] Succeeded') then
    return true
  elseif line:match('Serving') then
    return true
  end
end

-- [SEVERE] build_web_compilers:entrypoint on web/main.dart:
-- Dart2Js finished with:
--
-- packages/dpgw_ui/src/commons/ui/hp/matcher/hp_matcher.dart:20:2:
-- Error: Type 'vid' not found.
--  vid update() {
--  ^^^
-- Error: Compilation failed.
-- ...
-- [SEVERE] Failed after 11.4s
-- local function error(line)
-- end

local lastStatus
local function getStatus(line)
  if lastStatus and #line == 0 then
    return lastStatus
  end
  if (isSucceed(line)) then
    return ds.success
  end

  local lineType = line:match('^%[(.*)%] .*');
  if not lineType then
    return ds.running
  end

  if (lineType == 'SEVERE') then
    return ds.severe
  end

  return ds.running
end

M.onStart = function(data)
  data.tmpfile = '/tmp/' .. data.name .. '.trun'
  os.execute('rm -f ' .. data.tmpfile .. ' 2>/dev/null')
end
function M.handle(line, data)
  local tmpfile = io.open(data.tmpfile, 'a')
  tmpfile:write(line, '\n')
  tmpfile:close()
  local status = getStatus(line)
  lastStatus = status
  return {status, data.tmpfile}
end
M.onUpdate = function(_, status, data)
  if status[1] == ds.success then
    io.open(data.tmpfile, 'w'):close()
  end
  notifybar()
end
M.onEnd = function(data)
  notifybar()
  os.execute('rm -f ' .. data.tmpfile)
end

return M
