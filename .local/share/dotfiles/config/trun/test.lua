--- Trun module handler for `pub run`
--
-- interface
-- handle: return status that is written to a file
-- (optional) onStart: call once before start reading
-- (optional) onUpdate: calls every time when stdin reads a line
-- (optional) onEnd: calls once after stream end
local M = {}

-- handler
local ds = {['running'] = 0, ['success'] = 1, ['severe'] = -1}

-- update
local function notifybar() os.execute('bar-update trun') end

local function getStatus(line)
  if line:find('RESET') then
    return ds.success
  end
  if line:find('NORMAL') then
    return ds.running
  end
  return ds.severe
end

M.onStart = function(data)
  data.tmpfile = '/tmp/' .. data.name .. '.trun'
  os.execute('rm ' .. data.tmpfile .. ' 2>/dev/null')
end
M.handle = function(line, data)
  local tmpfile = io.open(data.tmpfile, 'a')
  tmpfile:write(line, '\n')
  tmpfile:close()
  return {getStatus(line), data.tmpfile}
end
M.onUpdate = function(_, status, data)
  if status[1] == ds.success then
    io.open(data.tmpfile, 'w'):close()
  end
  notifybar()
end
M.onEnd = function(data)
  notifybar()
  os.execute('rm ' .. data.tmpfile)
end

return M
