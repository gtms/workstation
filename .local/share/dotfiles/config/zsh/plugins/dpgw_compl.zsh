# Completion definition for dpgw.sh.
#     - DicomPass Gateway service script
#
# Author:
#   Tomáš Němec <nemec@medoro.org>
#

function _dpgw {
  local -a commands

  _arguments -C \
    '(-h --help)'{-h,--help}'[get help for dpgw]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
    cmnds)
      commands=(
        "start:starts server"
        "stop:stops server"
        "force_stop:force stops server"
        "restart:restarts server"
        "status:shows status of server"
        "ha:high-availability management"
        "system_init:manages system init scripts/units"
        "entity:manages aes, users, ..."
        "register:manages registers (patient, study, series, images)"
        "storage:manages stg groups, fs, ..."
        "route:manages routerules"
        "hl7clnt:hl7 client"
        "patsync:load patients (create/improve) from other dpgw by websocket"
        "uidgen:uid generator"
        "categorization_tag:manages categorization tags"
        "dpgw_cfg_fmt:format dpgw config file"
        "update:update dpgw config file"
        "dump:dumps memory or threads"
        "image_path:print pacs fsstorage file path by image uid"
        "cache_study_path:print all file paths of images in cache by study uid"
        "service:manage dpgw services (start, stop, status, list, reload)"
        "migrate_studies:migrate studies from another dicom ae"
        "migrate_storage:migrate data between storages"
        "so_download:retrieve storageobject to local file"
        "dcmchains_reload:reload dicom pluginchains configuration"
        "ldap_reload:reload ldap configuration and role mapping"
        "licenses_reload:reload licenses configuration"
        "token_keys_reload:reload dpgw token keys"
        "activation:activation request"
        "check_license:check for license changes"
        "reload_groovy_scripts:clear and reload groovy script cache"
        "reload_cloudpacs:reload cloudpacs config"
        "clear_login_attempts:clear login attempts counter"
        "cfg_validator:validates config file"
      )
      _describe "command" commands
      ;;
  esac

  case "$words[1]" in
    ha)
      _arguments \
        '-h[help]:' \
        '-c[_TODO_]: :(start stop status):'

      ;;
    system_init)
      _arguments \
        '-h[help]:' \
        '-c[_TODO_]: :(status install remove replace):'

      ;;
    entity)
      _arguments \
        '-h[help]:' \
        ': :(dicomae hl7ae user digistation)'

      ;;
    dump)
      _arguments \
        '-h[help]:' \
        '1: :(memory threads)' \
        ':filename:_files'

      ;;
    image_path)
      _arguments \
        '-h[help]:' \
        ':uid:'

      ;;

    cache_study_path)
      _arguments \
        '-h[help]:' \
        '1:uid:' \
        '2:cacheName:'

      ;;
    service)
      _arguments \
        '-h[help]:' \
        '1::servicename:' \
        ':: :(list status start stop reload)' \

      ;;
    cfg_validator)
      _arguments '::filename:_files'

      ;;
  esac
}

compdef _dpgw dpgw.sh
compdef _dpgw dpgw
