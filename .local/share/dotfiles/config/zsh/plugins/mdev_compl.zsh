#compdef _mdev mdev


function _mdev {
  local -a commands

  _arguments -C \
    '(-h --help)'{-h,--help}'[get help for mdev]' \
    "1: :->cmnds"

  case $state in
  cmnds)
    commands=(
      "start:Start app in tmux session"
      "restart:Rerun app"
      "term:Stop app"
      "open:Open tmux window at <app> position"
      "join:Join multiple app into single tmux window"
    )
    _describe "command" commands
    ;;
  esac
}
