# Workstation
My requirements for complete workstation.

## Shell
### Software
#### [zsh](https://www.zsh.org/)
### ENV

## GPG
TODO:
- ex/im secret
- trust

## SSH

## Terminal
### Software
#### [alacritty](https://github.com/alacritty/alacritty)
#### [foot](https://codeberg.org/dnkl/foot)

## Notifications
### Software
#### [duns](https://dunst-project.org/)
#### [herbe](https://github.com/dudik/herbe)

## Bar
### Needs
- Show windows of WM
```
```
- Show volume and output device
```
```
- Show keyboard layout
```
```
- Show Date&Time
```
```
### Other Needs
- Show New emails
```
```
- Show Toggl timer
```
```
- Show Timer countdown
```
```
- Show Trun status
```
```
- Show local/public IP
```
```
### Software
#### [lemonbar](https://github.com/LemonBoy/bar)

## Window Manager
### Needs
- Tiling layout
### Software
#### [bspwm](https://github.com/baskerville/bspwm)

## Keyboard shortcuts
### Software
#### [sxhkd](https://github.com/baskerville/sxhkd)

## Mail
### Needs
```
# SOLUTION
mail_check
```
- Print list of news emails per users
```
# SOLUTION
mail_count
```
### Scripts
- `mail_check`  
daemon to notify on new email
- `mail_count`  
list new emails per user

### Software
#### [Neomutt](https://neomutt.org)
Mail reader (Mail User Agent)
#### [isync/mbsync](https://isync.sourceforge.io/mbsync.html)
Synchronize IMAP4 and Maildir mailboxes
#### [msmtp](https://marlam.de/msmtp/)
SMTP client

## Editor
### Needs
- LSP
- Syntax highlighting
  - Treesitter

## IRC client
### Software
#### [tiny](https://github.com/osa1/tiny)

## RSS client
### Software
#### [sfeed](https://codemadness.org/sfeed.html)

## Passwords
### Needs
- Password manager (store/view/remove/generate/...)  
[Pass](https://www.passwordstore.org) 
- Quickly copy password  
```
passmenu
```
- Quickly copy section of password
```
pass-section [-c] <pass-name> <section>
# or via dmenu
passmenu_section
```

#### Scripts
- `pass-section [-c] <pass-name> <section>`  
Script for retrieve/copy section
- `passmenu`  
Dmenu for copy password to clipboard and notify of login
- `passmenu_section`  
Dmenu for copy section of password


## Web browsing
### Needs
- Bookmark manager (add/view/remove/tag/...)
```
TODO
```
- Open bookmark
```
# open bookmark via dmenu
bm
```

## Printing
### Needs
- Print  
[Cups](https://www.cups.org)
```
lp
```

### Software
#### [Cups](https://www.cups.org)
Used for actual printing
- daemon
- localhost:631 for web interface
```
# For print
lp [-o option=value] <file.pdf>
# For specifing default printer
lpadmin -d <name>
# For settins defaults and see printers specific options
lpoptions -l
```

#### [Avahi](https://github.com/lathiat/avahi)
Not sure if needed, it allows to discover locally plugged services/hosts with no specific configuration.
- daemon
