#!/bin/sh

cd ~/.fzf && git pull && ./install --xdg --no-bash --no-fish --no-update-rc
