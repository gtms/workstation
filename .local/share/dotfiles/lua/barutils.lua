local M = {}

-- print

M.D = function(fmt, ...) io.stderr:write(string.format("DEBUG: " .. fmt, ...), "\n") end
M.E = function(fmt, ...) io.stderr:write(string.format(fmt, ...), "\n") end

-- wrappers

M.w = {}
local w = M.w
w.btn = function(text, action, button) return string.format("%%{A%s:%s:}%s%%{A}", button or "", action, text or "") end
w.btns = function(text, t)
  for action, btn in pairs(t) do
    text = w.btn(text, action, btn)
  end
  return text
end
w.fg = function(text, color) return string.format("%%{F%s}%s%%{F-}", color, text) end
w.bg = function(text, color) return string.format("%%{B%s}%s%%{B-}", color, text) end
w.col = function(text, fg, bg)
  if bg then
    text = w.bg(text, bg)
  end
  if fg then
    text = w.fg(text, fg)
  end
  return text
end
w.u = function(text, color)
  if color then
    return string.format("%%{U%s}%%{+u}%s%%{-u}%%{U-}", color, text)
  else
    return string.format("%%{+u}%s%%{-u}", text)
  end
end
w.o = function(text, n) return string.format("%%{O%s}%s%%{U-}", n, text) end

-- utils

M.cmd_line = function(command)
  local handle = io.popen(command)
  local line = handle:read("l")
  handle:close()
  return line
end

M.monitors = function()
  local o = {}
  local xrandr = io.popen("xrandr --listmonitors")
  for line in xrandr:lines() do
    local pr, name = string.match(line, " %d+: %+(%*?).* (.*)")
    if name then
      table.insert(o, {name = name, primary = pr and pr ~= ""})
    end
  end
  xrandr:close()
  return o
end

return M
