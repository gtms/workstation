-- mbsync utilities modules
--
-- TODO: generalize path for mbsyncrc

local M = {}

-- List accounts inside mbsyncrc
M.accounts = function()
  local acc = {}
  for a in io.popen('cat ~/.mbsyncrc | sed -n \'s/^Group //p\''):lines() do
    table.insert(acc, a)
  end
  return acc
end

return M
