-- Neomutt profile module
-- Requires mutt global variable thus is only usable for neomutt
--
local utils = require('tms.neomutt.utils')

local profile = {}

profile.UNSET = '__unset__'

function profile:new(name, opts, def)
  def = def or {}
  self.__index = self

  local inner = {}
  setmetatable(inner, self)
  inner.__index = inner
  inner.options = def.options or {}
  inner.binds = def.binds or {}
  inner.macros = def.macros or {}
  inner.mailboxes = def.mailboxes or {}

  local p = {}
  setmetatable(p, inner)
  p.name = name
  p.options = opts.options
  p.binds = opts.binds
  p.macros = opts.macros
  p.mailboxes = opts.mailboxes
  return p
end

function profile:load()
  mutt.message(string.format('loading %s', self.name))
  for k, v in pairs(self.options) do
    if v == profile.UNSET then
      utils.unset(k)
    else
      utils.set {k, v}
    end
  end
  for _, v in pairs(self.binds) do
    utils.bind(table.unpack(v))
  end
  for _, v in pairs(self.macros) do
    utils.macro(table.unpack(v))
  end
  mutt.enter(string.format('mailboxes %s', table.concat(self.mailboxes, ' ')))
end

function profile:unload()
  mutt.command.unmailboxes('*')
  for k, _ in pairs(self.options) do
    utils.unset(k)
  end
  for _, v in pairs(self.binds) do
    utils.unbind(table.unpack(v))
  end
  for _, v in pairs(self.macros) do
    utils.unmacro(table.unpack(v))
  end
end

return profile
