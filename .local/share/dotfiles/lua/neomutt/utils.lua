-- Neomutt utilities module
-- Make setting and mapping easy
-- Requires mutt global variable thus is only usable for neomutt
--
local M = {}

function Dump(t, indent)
  indent = indent or 0
  local s = '>'
  for k, v in pairs(t) do
    print(s:rep(indent), k, v)
    if type(v) == 'table' then
      Dump(v, indent + 1)
    end
  end
end

-- SETTINGS / OPTIONS
M.options = {}
local settings_mt = {
  __index = function(_, k) return mutt.get(k) end,
  __newindex = function(_, k, v) mutt.set(k, v) end,
}
setmetatable(M.options, settings_mt)

M.set = function(option)
  if type(option) == 'string' then
    M.options[option] = true
  elseif type(option) == 'table' then
    mutt.set(option[1], option[2])
    -- M.options[option[1]] = option[2]
  end
end

M.unset = function(option) mutt.command.unset(option) end

-- KEYBIND
local menu = {
  ['al'] = 'alias',
  ['a'] = 'attach',
  ['b'] = 'browser',
  ['c'] = 'compose',
  ['e'] = 'editor',
  ['g'] = 'generic',
  ['i'] = 'index',
  ['m'] = 'mix',
  ['p'] = 'pager',
  ['pg'] = 'pgp',
  ['po'] = 'postpone',
  ['q'] = 'query',
  ['s'] = 'smime',
}
function string:split(sep)
  local fields
  sep, fields = sep or ':', {}
  local pattern = string.format('([^%s]+)', sep)
  self:gsub(pattern, function(c) fields[#fields + 1] = c end)
  return fields
end

local function menu_expand(m)
  if m == '*' then
    return m
  end
  local sep = ','
  local res = {}
  if m:find(sep) then
    for _, key in pairs(menu:split(sep)) do
      if menu[key] then
        table.insert(res, menu[key])
      end
    end
  else
    for key in m:gmatch '.' do
      if menu[key] then
        table.insert(res, menu[key])
      end
    end
  end
  return table.concat(res, sep)
end
M.bind = function(menu, key, fn) mutt.command.bind(menu_expand(menu), key, fn) end
M.unbind = function(menu, key) mutt.command.unbind(menu_expand(menu), key) end
M.macro = function(menu, key, fn)
  mutt.command.macro(menu_expand(menu), key, string.format('\'%s\'', fn))
end
M.unmacro = function(menu, key) mutt.command.unmacro(menu_expand(menu), key) end
return M
